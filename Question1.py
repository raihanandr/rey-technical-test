# Generated by Selenium IDE
import pytest
import time
import json
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

class TestSearchPokemonwithnamePikachu():
  def setup_method(self, method):
    self.driver = webdriver.Chrome()
    self.vars = {}
  
  def teardown_method(self, method):
    self.driver.quit()
  
  def test_searchPokemonwithnamePikachu(self):
    self.driver.get("https://www.pokemon.com/us")
    self.driver.set_window_size(1382, 784)
    self.driver.find_element(By.CSS_SELECTOR, "a > .icon_pokeball").click()
    self.driver.find_element(By.ID, "searchInput").click()
    self.driver.find_element(By.ID, "searchInput").send_keys("Pikachu")
    self.driver.find_element(By.ID, "search").click()
    self.driver.find_element(By.CSS_SELECTOR, "figure img").click()
    WebDriverWait(self.driver, 30).until(expected_conditions.visibility_of_element_located((By.CSS_SELECTOR, ".pokedex-pokemon-share > .column-12")))
    self.driver.find_element(By.LINK_TEXT, "Explore More Pokémon").click()
    WebDriverWait(self.driver, 30).until(expected_conditions.visibility_of_element_located((By.ID, "loadMore")))
    self.driver.close()
  
